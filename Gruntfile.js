module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dist: {
                src: ['src/js/*.js', 'src/vendor/js/*.js'],
                dest: 'templates/corporate/js/build.js'
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            dist: {
                files: {
                    'templates/corporate/js/build.min.js': ['templates/corporate/js/build.js']
                }
            }
        },
        jshint: {
            files: ['gruntfile.js', 'src/js/*.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },
        less: {
            production: {
                options: {
                    cleancss: true
                },
                files: {
                    "templates/corporate/css/style.min.css": ["src/less/*.less", "src/vendor/less/*.less"]
                }
            }

        },
        jade: {
            compile: {
                options: {
                    pretty: true,
                    data: {
                        //debug: false
                    }
                },
                files: {
                    "index.html": ["src/jade/index.jade"],
                    "mult.html": ["src/jade/mult.jade"],
                    "specpre.html": ["src/jade/specpre.jade"],
                    "distribution.html": ["src/jade/distribution.jade"],
                    "license.html": ["src/jade/license.jade"],
                    "contacts.html": ["src/jade/contacts.jade"],
                    "mimimishki.html": ["src/jade/mimimishki.jade"],
                    "arkadij-parovozov.html": ["src/jade/ap.jade"],
                    "bumazhki.html": ["src/jade/bumazhki.jade"],
                    "mama.html": ["src/jade/mama.jade"],
                    "ani.html": ["src/jade/ani.jade"],
                    "lp.html": ["src/jade/lp.jade"],
                    "dv.html": ["src/jade/dv.jade"],
                }
            }
        },
        watch: {
            files: ['<%= concat.dist.src %>', 'src/**/*.js', 'src/**/*.less', 'src/**/*.jade'],
            tasks: ['jshint','less','jade','concat','uglify']
        }
    });


    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jade');

    grunt.registerTask('default', ['jshint', 'concat', 'uglify','less']);
};