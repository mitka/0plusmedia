/*
    1. У каждого слайдера должен быть какой-нибудь id
    2. Каждый слайдер должен иметь класс knf-slider
    3. Класс animated означает, что данный слайдер должен прокручиваться с анимацией
    4. Класс work-mode-on означает, что данный слайдер прокручивается, отсутствие этого класса означает. что слайдер не двигается
*/
function knfSlider(config) {
    if (!!!config) return;
    if (!!config.element) {
        //console.log('knfSlider');
        //console.log(config.workMode);
        var _self = this;
        
        _self.hasClass = function(className) {
            return _self.element.className.indexOf(className); 
        };
        
        _self.element = config.element; // DOM-объект слайдера
        _self.interval = config.timeout || 10000; // интервал пролистывания
        _self.workMode = (Number(_self.hasClass('work-mode-on')) != -1) ? true : false; // будет ли слайдер вообще пролистываться
        _self.animationMode = (Number(_self.hasClass('animated')) != -1) ? 'on' : 'off'; // анимирование движения

        _self.slides = document.querySelectorAll('#' + _self.element.id + ' .slide');
        _self.slidesNum = _self.slides.length; //Количство слайдов
        _self.currentSlideIndex = -1; //Номер текущего слайда, стартовое значение 0
        _self.slideTape = document.querySelector('#' + _self.element.id + ' .slides');
        _self.slideTapeMargin = '0px'; //смещение ленты слайдов
        _self.rightEnd = false; //флаг - поднимается когда надо отмотать слайды назад
        _self.leftEnd = false;
        _self.moveTimer = 0;
        _self.timer = 0;
        _self.youCanMoveIt = false;

        /*_self.engine = function() {
            console.log('врум-врум, мазафака!');
        };*/


        document.querySelectorAll('#' + _self.element.id + ' .slider-arrow-left')[0].onclick = function() {
            //console.log('влево');
            _self.moveByArrow('left');
        };
        document.querySelectorAll('#' + _self.element.id + ' .slider-arrow-right')[0].onclick = function() {
            //console.log('вправо');
            _self.moveByArrow('right');
        };

        _self.slideTape.style.marginLeft = _self.slidesNum * -230 + 'px';
        var slidersHtml = document.querySelector('#' + _self.element.id + ' .slides').innerHTML;
        slidersHtml = slidersHtml + slidersHtml + slidersHtml;
        document.querySelector('#' + _self.element.id + ' .slides').innerHTML = slidersHtml;
        
        
        
        
        // автоматический слайдинг
        _self.regularMove = function() {
            clearInterval(_self.timer);
            _self.youCanMoveIt = false;
            var path = Number(_self.slideTapeMargin - 231); // сколько подвинуть
            var f = _self.slideTapeMargin,
                s = 5;
            _self.moveTimer = setInterval(function() {
                f += -1 * s;
                if (_self.animationMode != 'off') {
                    _self.slideTape.style.marginLeft = f - 5 + 'px';
                    if (f <= path + 10) {
                        clearInterval(_self.moveTimer);
                        _self.engine();
                    }
                } else {
                    _self.slideTape.style.marginLeft = path + 1;
                    clearInterval(_self.moveTimer);
                    _self.engine();
                }
            }, 20);
        };

        // смена слайда по стрелке
        _self.moveByArrow = function(direction) {
            if (!_self.youCanMoveIt) {
                return false;
            }
            _self.youCanMoveIt = false;
            clearInterval(_self.moveTimer);
            clearInterval(_self.timer);
            //console.log('direction = ' + direction + '; _self.slideTapeMargin = ' + _self.slideTapeMargin);
            var path, x;
            if (direction == 'left') {
                path = _self.slideTapeMargin - 231;
                x = -1;
            } else {
                path = _self.slideTapeMargin + 231;
                x = 1;
            }
            //console.log('подвинем к отметке ' + path);
            var f = _self.slideTapeMargin,
                s = 10;
            _self.moveTimer = setInterval(function() {
                f += x * s;

                if (_self.animationMode != 'off') {
                    _self.slideTape.style.marginLeft = f - (x * 10) + 'px';
                    if (direction == 'left') {
                        if (f <= path) {
                            clearInterval(_self.moveTimer);
                            _self.engine(direction);
                        }
                    } else {
                        if (f >= path) {
                            clearInterval(_self.moveTimer);
                            _self.engine(direction);
                        }
                    }
                } else {
                    _self.slideTape.style.marginLeft = path - x;
                    clearInterval(_self.moveTimer);
                    _self.engine(direction);
                }
            }, 20);
        };

        //функция учета текущего состояния слайдера
        _self.engine = function(t) {
            _self.youCanMoveIt = true;
            var v = getComputedStyle(_self.slideTape).marginLeft,
                direction = t || 'left';
            clearInterval(_self.timer);
            //console.log('[Slider.engine] Проворачиваем механизм ' + direction);
            _self.slideTapeMargin = Number(v.substr(0, v.indexOf('px')));
            //console.log('[Slider.engine] slideTapeMargin = ' + _self.slideTapeMargin);
            if (_self.currentSlideIndex == _self.slidesNum - 1) {
                //console.log('[Slider.engine] предел справа');
                _self.currentSlideIndex = 0;
                _self.rightEnd = true;
            } else if (_self.currentSlideIndex == _self.slidesNum - 2 * _self.slidesNum + 1) {
                //console.log('[Slider.engine] предел слева');
                _self.currentSlideIndex = 0;
                _self.leftEnd = true;
            } else {
                if (direction == 'left') {
                    _self.currentSlideIndex++;
                } else {
                    _self.currentSlideIndex--;
                }
            }

            //запускаем анимацию
            _self.timer = setInterval(function() {
                _self.regularMove();
            }, _self.interval);

            // перемотка
            setTimeout(function() {
                if (_self.rightEnd) {
                    //console.log('[test] end of cicle1');
                    _self.currentSlideIndex = 0;
                    _self.slideTapeMargin = _self.slidesNum * -230;
                    _self.slideTape.style.marginLeft = _self.slidesNum * -230 + 'px';
                    _self.rightEnd = false;
                }
                if (_self.leftEnd) {
                    //console.log('[test] end of cicle2');
                    _self.currentSlideIndex = 0;
                    _self.slideTapeMargin = _self.slidesNum * -230;
                    _self.slideTape.style.marginLeft = _self.slidesNum * -230 + 'px';
                    _self.leftEnd = false;
                }
            }, 10);
        };
        console.log(_self.timeout);
        if(!!_self.workMode) {
            _self.engine();
        }
    }
}
function knfInit() {
    if(document.getElementsByClassName('knf-slider').length > 0) {
        var sliderElements = document.getElementsByClassName('knf-slider');
        for(var i = 0; i < sliderElements.length; i++) {
            new knfSlider({
                "timeout": 10000,
                "element": sliderElements[i]
            });
        }
    } else {
        return false;
    }
}