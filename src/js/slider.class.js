function Slider(config) {
        
    if(!!!config) return ;
    if(!!config.element) {
        
        var _self = this,
            elem = config.element.id;
            
        //if(!!!elem) return false;
        
        this.engine = function(){};
        this.mode = config.mode; // в режиме off анимация отключается
        this.interval = config.timeout || 5000; //Интервал перелистывания
        //this.slides = document.getElementsByClassName('slide'); //Массив слайдов
        this.slides = document.querySelectorAll('#' + elem + ' .slide');
        this.slidesNum = this.slides.length; //Количство слайдов
        this.currentSlideIndex = -1; //Номер текущего слайда, стартовое значение 0
        //this.slideTape = document.getElementsByClassName('slides')[0];
        this.slideTape = document.querySelector('#' + elem + ' .slides');
        this.slideTapeMargin = '0px'; //смещение ленты слайдов
        this.rightEnd = false; //флаг - поднимается когда надо отмотать слайды назад
        this.leftEnd = false;
        this.moveTimer = 0;
        this.timer = 0;
        this.youCanMoveIt = false;
        /*console.log('interval = ' + _self.interval + '; slides = ' + _self.slides + '; slidesNum = ' + _self.slidesNum +
            '; currentSlideIndex = ' + _self.currentSlideIndex + '; slideTape = ' + _self.slideTape +
            '; slideTapeMargin = ' + _self.slideTapeMargin + '; rightEnd = ' + _self.rightEnd + ';config.element.id = ' + config.element.id);*/
        document.querySelectorAll('#' + elem + ' .slider-arrow-left')[0].onclick = function() {
            //console.log('влево');
            _self.moveByArrow('left');
        };
        document.querySelectorAll('#' + elem + ' .slider-arrow-right')[0].onclick = function() {
            //console.log('вправо');
            _self.moveByArrow('right');
        };
        
        _self.slideTape.style.marginLeft = _self.slidesNum*-230+'px';
        var slidersHtml = document.querySelector('#' + elem + ' .slides').innerHTML;
        slidersHtml = slidersHtml+slidersHtml+slidersHtml;
        document.querySelector('#' + elem + ' .slides').innerHTML = slidersHtml;
        
        // автоматический слайдинг
        this.regularMove = function() {
            clearInterval(_self.timer);
            _self.youCanMoveIt = false;
            var path = Number(_self.slideTapeMargin - 231); // сколько подвинуть
            var f = _self.slideTapeMargin,
                s = 5;
            _self.moveTimer = setInterval(function() {
                f += -1 * s;
                if(_self.mode!='off') {
                    _self.slideTape.style.marginLeft = f - 5 + 'px';
                    if (f <= path + 10) {
                        clearInterval(_self.moveTimer);
                        _self.engine();
                    }
                } else {
                    _self.slideTape.style.marginLeft = path+1;
                    clearInterval(_self.moveTimer);
                    _self.engine();
                }
            }, 20);
        };
    
        // смена слайда по стрелке
        this.moveByArrow = function(direction) {
            if (!_self.youCanMoveIt) {
                return false;
            }
            _self.youCanMoveIt = false;
            clearInterval(_self.moveTimer);
            clearInterval(_self.timer);
            //console.log('direction = ' + direction + '; _self.slideTapeMargin = ' + _self.slideTapeMargin);
            var path, x;
            if (direction == 'left') {
                path = _self.slideTapeMargin - 231;
                x = -1;
            } else {
                path = _self.slideTapeMargin + 231;
                x = 1;
            }
            //console.log('подвинем к отметке ' + path);
            var f = _self.slideTapeMargin,
                s = 10;
            _self.moveTimer = setInterval(function() {
                f += x * s;
                
                if(_self.mode!='off') {
                    _self.slideTape.style.marginLeft = f - (x * 10) + 'px';
                    if (direction == 'left') {
                        if (f <= path) {
                            clearInterval(_self.moveTimer);
                            _self.engine(direction);
                        }
                    } else {
                        if (f >= path) {
                            clearInterval(_self.moveTimer);
                            _self.engine(direction);
                        }
                    }
                } else {
                    _self.slideTape.style.marginLeft = path-x;
                    clearInterval(_self.moveTimer);
                    _self.engine(direction);
                }
            }, 20);
        };
    
        //функция учета текущего состояния слайдера
        this.engine = function(t) {
            _self.youCanMoveIt = true;
            var v = getComputedStyle(_self.slideTape).marginLeft,
                direction = t || 'left';
            clearInterval(_self.timer);
            //console.log('[Slider.engine] Проворачиваем механизм ' + direction);
            this.slideTapeMargin = Number(v.substr(0, v.indexOf('px')));
            //console.log('[Slider.engine] slideTapeMargin = ' + this.slideTapeMargin);
            if (this.currentSlideIndex == this.slidesNum - 1) {
                //console.log('[Slider.engine] предел справа');
                this.currentSlideIndex = 0;
                this.rightEnd = true;
            } else if(this.currentSlideIndex == this.slidesNum - 2*this.slidesNum+1) {
                //console.log('[Slider.engine] предел слева');
                this.currentSlideIndex = 0;
                this.leftEnd = true;
            } else {
                if (direction == 'left') {
                    this.currentSlideIndex++;
                } else {
                    this.currentSlideIndex--;
                }
            }
    
            //запускаем анимацию
            _self.timer = setInterval(function() {
                _self.regularMove();
            }, _self.interval);
    
            // перемотка
            setTimeout(function() {
                if (_self.rightEnd) {
                    //console.log('[test] end of cicle1');
                    _self.currentSlideIndex = 0;
                    _self.slideTapeMargin = _self.slidesNum*-230;
                    _self.slideTape.style.marginLeft = _self.slidesNum*-230+'px';
                    _self.rightEnd = false;
                }
                if(_self.leftEnd) {
                    //console.log('[test] end of cicle2');
                    _self.currentSlideIndex = 0;
                    _self.slideTapeMargin = _self.slidesNum*-230;
                    _self.slideTape.style.marginLeft = _self.slidesNum*-230+'px';
                    _self.leftEnd = false;
                }
            }, 10);
        };
    }
}